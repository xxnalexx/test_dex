<?php

namespace App\Services;

use App\DTO\Order\OrderDTOInterface;
use App\Order;

class OrderService
{
    /**
     * @param OrderDTOInterface $orderDTO
     * @return mixed
     */
    public function updateStatus(OrderDTOInterface $orderDTO)
    {
        $order = Order::find($orderDTO->getId());

        if (empty($order)) {
            throw new \RuntimeException("Order not found");
        }

        $order->status = $orderDTO->getStatus();
        $order->save();

        return $order;
    }
}
