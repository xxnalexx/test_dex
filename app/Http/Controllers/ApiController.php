<?php

namespace App\Http\Controllers;

use App\DTO\Order\GatewayOrderDTO;
use App\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;

class ApiController
{

    /**
     * @param Request $request
     * @param OrderService $orderService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(Request $request, OrderService $orderService)
    {
        try {
            $order = $orderService->updateStatus(
                new GatewayOrderDTO($request->order)
            );

            if ($order->status == Order::STATUS_COMPLETE) {
                return redirect()->route('gateway.success');
            }

            return redirect()->route('gateway.fail');

        } catch (\Exception $exception) {
            \Log::error('GatewayCallback: ', ['message' => $exception->getMessage(), 'context' => $exception->getTraceAsString()]);

            return redirect()->route('gateway.fail');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success()
    {
        return view('gateway.success');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fail()
    {
        return view('gateway.fail');
    }
}
