<?php

namespace App\DTO\Order;

use App\Order;

class GatewayOrderDTO implements OrderDTOInterface
{
    private $id;
    private $status;

    public function __construct(array $order)
    {
        $this
            ->setId($order['order_id'])
            ->setStatus($this->processStatus($order['status']));
    }

    /**
     * Casting response order status to system order status
     *
     * @param string $orderStatus
     * @return string
     */
    private function processStatus(string $orderStatus)
    {
        switch($orderStatus)
        {
            case "declined": {
                return Order::STATUS_DECLINED;
            }
            case "complete": {
                return Order::STATUS_COMPLETE;
            }
        }
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getId()
    {
        return $this->id;
    }
}
