<?php

namespace App\DTO\Order;

interface OrderDTOInterface
{
    public function getId();
    public function getStatus();
}
