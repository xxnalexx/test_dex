<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $keyType = 'string';
    public $incrementing = false;

    const STATUS_COMPLETE = 'complete';
    const STATUS_DECLINED = 'declined';

}
