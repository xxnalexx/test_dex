<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'gateway', 'as' => 'gateway.'], function() {
    Route::match(['GET', 'POST'], '/callback', 'ApiController@callback');
    Route::get('/success', 'ApiController@success')->name('success');
    Route::get('/fail', 'ApiController@fail')->name('fail');
});
